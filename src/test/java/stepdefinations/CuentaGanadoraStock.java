package stepdefinations;

import java.io.File;


import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utility.Hook;

public class CuentaGanadoraStock {

	private WebDriver driver;
	String baseUrl;
	WebElement element;
	
	public CuentaGanadoraStock() {
		this.driver = Hook.getDriver();
	}
	
	@Given("^Abrir el Formulario de cuentas$")
	public void abrir_el_Formulario_de_cuentas() throws Throwable {
		//baseUrl = "https://extranetdev.grupobbva.pe/cuentas";
		baseUrl = "http://localhost:8095/cuentas/";
	    driver.get(baseUrl);
	}

	@When("^Ingreso Dni y datos de contacto y se hace click en boton empezar$")
	public void ingreso_Dni_y_datos_de_contacto_y_se_hace_click_en_boton_empezar() throws Throwable {
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.id("txtDatoBasicoNumDocumento")).clear();
		driver.findElement(By.id("txtDatoBasicoNumDocumento")).sendKeys("12345678");
		driver.findElement(By.id("txtDatoBasicoCorreo")).clear();
		driver.findElement(By.id("txtDatoBasicoCorreo")).sendKeys("moraivanm@gmail.com");
		driver.findElement(By.id("txtDatoBasicoTelefono")).clear();
		driver.findElement(By.id("txtDatoBasicoTelefono")).sendKeys("966707745");
		element = driver.findElement(By.id("txtDatoBasicoTelefono"));
		element.sendKeys(Keys.TAB);
		File scrFile1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1, new File("c:\\test\\cuentas\\Paso1.JPG"));

		element = (new WebDriverWait(driver, 30))
		.until(ExpectedConditions.elementToBeClickable(By.id("btnContinuarPaso1")));
		element.click();

		Thread.sleep(5000);
	}

	@When("^Seleccionar pesta?a de cuenta Ganadora y seleccionar la Opcion Si$")
	public void seleccionar_pesta_a_de_cuenta_Ganadora_y_seleccionar_la_Opcion_Si() throws Throwable {
	
		// Seleccionar pesta�a ganadora
		element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(
		By.xpath("//*[@id='seccion-02']/div/div[1]/div[3]/div[2]/div/div/div[2]/div[4]")));
		element.click();
		Thread.sleep(1000);
		//Seleccion de Premio
		
		element = driver.findElement(By.xpath("//*[@id='formTipoVia']/div/button"));
		element.click();
		Thread.sleep(1000);
		element = driver
		.findElement(By.xpath("//*[@id='formTipoVia']/div/div/ul/li[1]/a/span[1]"));
		element.click();
		Thread.sleep(1000);
	}

	@Then("^Validar que se muestre premios dentro del combo$")
	public void validar_que_se_muestre_premios_dentro_del_combo() throws Throwable {
	
		
	}

}
