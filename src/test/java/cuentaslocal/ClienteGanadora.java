package cuentaslocal;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.io.FileUtils;

public class ClienteGanadora {
	WebDriver driver;
	WebElement element;
	String baseUrl;
	
	@BeforeTest
	public void setUp() {
	// init your webdriver
		System.setProperty("webdriver.chrome.driver", "C:\\driver2\\chromedriver.exe");
	    driver = new ChromeDriver();
	}
	
	@Test
	public void flujoPrincipal() throws Exception {
//	  baseUrl = "http://localhost:8095/cuentas/";
	  baseUrl = "https://extranetdev.grupobbva.pe/cuentas";
		driver.get(baseUrl);
		driver.manage().window().maximize();
		// Pantalla Inicial

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.id("txtDatoBasicoNumDocumento")).clear();
		driver.findElement(By.id("txtDatoBasicoNumDocumento")).sendKeys("12345678");
		driver.findElement(By.id("txtDatoBasicoCorreo")).clear();
		driver.findElement(By.id("txtDatoBasicoCorreo")).sendKeys("moraivanm@gmail.com");
		driver.findElement(By.id("txtDatoBasicoTelefono")).clear();
		driver.findElement(By.id("txtDatoBasicoTelefono")).sendKeys("966707745");
		element = driver.findElement(By.id("txtDatoBasicoTelefono"));
		element.sendKeys(Keys.TAB);
		File scrFile1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1, new File("c:\\test\\cuentas\\Paso1.JPG"));

		element = (new WebDriverWait(driver, 30))
				.until(ExpectedConditions.elementToBeClickable(By.id("btnContinuarPaso1")));
		element.click();

		Thread.sleep(5000);
		// Seleccion de cuentas

		// Seleccionar pesta�a ganadora
		element = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(
				By.xpath("//*[@id='seccion-02']/div/div[1]/div[3]/div[2]/div/div/div[2]/div[4]")));
		element.click();
		Thread.sleep(1000);
		// driver.findElement(By.xpath("//div[1]/div/div[2]/div/div[1]/div[3]/div[2]/div/div/div[2]/div[4]/div/div")).click();
		element = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.id("btnContinuarPaso2")));
		element.click();
		File scrFile1x = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1x, new File("c:\\test\\cuentas\\Paso1x.JPG"));
		Thread.sleep(2000);

		// Identidad y ubicacion
		element = driver.findElement(By.id("confirmacionFatcaSi"));
		element.click();

		element = driver.findElement(By.id("chkDireccion2"));
		element.click();

		File scrFile2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile2, new File("c:\\test\\cuentas\\Paso2.JPG"));

		element = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btnContinuarPasodireccion']")));
		element.click();

		// Preguntas Candado

		// Pregunta 1
		element = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='txt-lbl-pregunta1']")));
		String p1 = element.getText();
		System.out.println(p1);

		if (p1 != null) {

			if (p1.equals("Ingresa el nombre del departamento donde naciste")) {
				element = driver.findElement(By.cssSelector("[data-id='cmbDepartamento1']"));
				Thread.sleep(1000);
				element.click();
				Thread.sleep(1000);
				element = driver.findElement(By.xpath("//*[@id='cmb-pregunta1']/div[2]/div/div/ul/li[15]/a"));
				element.click();
			} else if (p1.equals("Ingresa el primer nombre de tu padre")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta1']")).sendKeys("jose");
			}

			else if (p1.equals("Ingresa el primer nombre de tu madre")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta1']")).sendKeys("maria");
			}

			else if (p1.equals("Ingresa el nombre de la provincia donde naciste")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta1']")).sendKeys("lima");
			}

			else if (p1.equals("Ingresa el nombre del distrito donde naciste")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta1']")).sendKeys("barranco");
			}

			else {
				System.out.println("Ningun texto conincide con la pregunta candado 1");
			}
		} else {
			System.out.println("No se encontro el elemento");
		}

		// Pregunta 2

		element = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='txt-lbl-pregunta2']")));
		String p2 = element.getText();
		System.out.println(p2);

		if (p2 != null) {

			if (p2.equals("Ingresa el nombre del departamento donde naciste")) {
				element = driver.findElement(By.cssSelector("[data-id='cmbDepartamento2']"));
				Thread.sleep(1000);
				element.click();
				Thread.sleep(1000);
				element = driver.findElement(By.xpath("//*[@id='cmb-pregunta2']/div[2]/div/div/ul/li[15]/a"));
				element.click();
			} else if (p2.equals("Ingresa el primer nombre de tu padre")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta2']")).sendKeys("jose");
			}

			else if (p2.equals("Ingresa el primer nombre de tu madre")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta2']")).sendKeys("maria");
			}

			else if (p2.equals("Ingresa el nombre de la provincia donde naciste")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta2']")).sendKeys("lima");
			}

			else if (p2.equals("Ingresa el nombre del distrito donde naciste")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta2']")).sendKeys("barranco");
			}

			else {
				System.out.println("Ningun texto conincide con la pregunta candado 2");
			}
		} else {
			System.out.println("No se encontro el elemento");
		}

		// Pregunta 3
		element = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='txt-lbl-pregunta3']")));
		String p3 = element.getText();
		System.out.println(p3);

		if (p3 != null) {

			if (p3.equals("Ingresa el nombre del departamento donde naciste")) {
				element = driver.findElement(By.cssSelector("[data-id='cmbDepartamento3']"));
				Thread.sleep(1000);
				element.click();
				Thread.sleep(1000);
				element = driver.findElement(By.xpath("//*[@id='cmb-pregunta3']/div[2]/div/div/ul/li[15]/a"));
				element.click();
			} else if (p3.equals("Ingresa el primer nombre de tu padre")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta3']")).sendKeys("jose");
			}

			else if (p3.equals("Ingresa el primer nombre de tu madre")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta3']")).sendKeys("maria");
			}

			else if (p3.equals("Ingresa el nombre de la provincia donde naciste")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta3']")).sendKeys("lima");
			}

			else if (p3.equals("Ingresa el nombre del distrito donde naciste")) {
				driver.findElement(By.xpath("//*[@id='txt-pregunta3']")).sendKeys("barranco");
			}

			else {
				System.out.println("Ningun texto conincide con la pregunta candado 3");
			}
		} else {
			System.out.println("No se encontro el elemento");
		}

		// Pregunta Fatca

		element = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.id("lblRadNacionalidadCargoPublicoNo")));
		element.click();

		// boton siguiente paso
		File scrFile3 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile3, new File("c:\\test\\cuentas\\Paso3.JPG"));

		Thread.sleep(2000);
		driver.findElement(By.id("btnContinuarPasocandado")).click();

		Thread.sleep(5000);

		// Pantalla Datos de ocupaci�n

		element = driver.findElement(By.cssSelector("[data-id='cmbOcupacion']"));
		Thread.sleep(5000);
		element.click();
		Thread.sleep(2000);
		element = driver
				.findElement(By.xpath("//*[@id='seccion-05']/div/div/div[3]/div[2]/div[4]/div[1]/div/div/ul/li[1]/a"));
		element.click();

		element = driver.findElement(By.cssSelector("[data-id='cmbRubro']"));
		// Thread.sleep(1000);
		element.click();
		// Thread.sleep(1000);
		element = driver
				.findElement(By.xpath("//*[@id='seccion-05']/div/div/div[3]/div[2]/div[4]/div[3]/div/div/ul/li[1]/a"));
		element.click();

		driver.findElement(By.xpath("//*[@id='txtEmpresa']")).sendKeys("MDP");
		;

		driver.findElement(By.xpath("//*[@id='btnContinuarPasodatoslaborales']")).click();

		File scrFile4 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile4, new File("c:\\test\\cuentas\\Paso4.JPG"));
		Thread.sleep(3000);

		// Pantalla Terminos y condiciones

		driver.findElement(By.xpath("//*[@id='chkTerminosCondicionesClausulasContratacion2']/i")).click();

		driver.findElement(By.xpath("//*[@id='sms-digito-1']")).sendKeys("0");
		driver.findElement(By.xpath("//*[@id='sms-digito-2']")).sendKeys("0");
		driver.findElement(By.xpath("//*[@id='sms-digito-3']")).sendKeys("0");
		driver.findElement(By.xpath("//*[@id='sms-digito-4']")).sendKeys("0");
		driver.findElement(By.xpath("//*[@id='sms-digito-5']")).sendKeys("0");
		driver.findElement(By.xpath("//*[@id='sms-digito-6']")).sendKeys("0");

		element = driver.findElement(By.xpath("//*[@id='sms-digito-6']"));
		element.sendKeys(Keys.TAB);

		driver.findElement(By.id("btnContinuarPaso6")).click();

		File scrFile5 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile5, new File("c:\\test\\cuentas\\Paso5.JPG"));

		Thread.sleep(3000);
		// Assertt
		File scrFile6 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile6, new File("c:\\test\\cuentas\\Paso6.JPG"));

		String cuentaGanadora = "Cuenta Ganadora - Soles";
		String descriptionTextXPath = "//*[@id='seccion-07']/div/div/div/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div";
		WebElement Element1 = driver.findElement(By.xpath(descriptionTextXPath));
		String descriptionText = Element1.getAttribute("innerText");
		Assert.assertEquals(cuentaGanadora, descriptionText);
		System.out.println(descriptionText);

	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.close();
	}

}
