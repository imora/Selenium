package tatlocal;

import org.testng.Assert;


import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebElement;


public class ClienteSinMotor {
	WebDriver driver;
	WebElement element;
	String baseUrl;
	
	@BeforeTest
	public void setUp() {
	// init your webdriver
		System.setProperty("webdriver.chrome.driver", "C:\\drive1\\chromedriver.exe");
		
		 driver = new ChromeDriver();
	     DesiredCapabilities capabilities = new DesiredCapabilities();
	     capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	     
	}
	
	@Test
	public void flujoPrincipal() throws Exception {
	    baseUrl = "http://localhost:8092/tarjetas/";
      //baseUrl = "https://extranetdev.grupobbva.pe/tarjetas";
		driver.get(baseUrl);
        //driver.manage().window().maximize();
	    // Pantalla Inicial

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.id("txtNumeroDocumento")).clear();
		driver.findElement(By.id("txtNumeroDocumento")).sendKeys("15628080");
		driver.findElement(By.id("txtCorreo")).clear();
		driver.findElement(By.id("txtCorreo")).sendKeys("moraivanm@gmail.com");
		element=driver.findElement(By.id("txtCorreo"));
		element.sendKeys(Keys.TAB);
	    element = (new WebDriverWait(driver,10))
	   .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btnContinuarPaso0']")));
	    element.click();
		File scrFile1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1, new File("c:\\test\\tarjetas\\Paso1.JPG"));

		Thread.sleep(3000);
		
		//Pantalla Preguntas candado
	    
	    driver.findElement(By.xpath("//*[@id='fieldset0']/div[1]/label/label")).click();
	    driver.findElement(By.xpath("//*[@id='fieldset1']/div[1]/label/label")).click();
	    driver.findElement(By.xpath("//*[@id='fieldset2']/div[1]/label/label")).click();
	    
	    driver.findElement(By.id("txtTelefono")).sendKeys("966707377");
	    driver.findElement(By.id("btnEnviarSms")).click();
	    
	    Thread.sleep(2000);
	    
	    element = (new WebDriverWait(driver,10))
	    .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='sms-digito-1']")));
	        	    
	    element= driver.findElement(By.id("sms-digito-1"));
	    element.sendKeys("0");
	    element= driver.findElement(By.id("sms-digito-2"));
	    element.sendKeys("0");
	    element= driver.findElement(By.id("sms-digito-3"));
	    element.sendKeys("0");
	    element= driver.findElement(By.id("sms-digito-4"));
	    element.sendKeys("0");
	    element= driver.findElement(By.id("sms-digito-5"));
	    element.sendKeys("0");
	    element= driver.findElement(By.id("sms-digito-6"));
	    element.sendKeys("0");
	    
	    driver.findElement(By.id("sms-digito-6")).sendKeys(Keys.TAB);

	    File scrFile2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	  	FileUtils.copyFile(scrFile2, new File("c:\\test\\tarjetas\\Paso2.JPG"));
	    
	    element = (new WebDriverWait(driver,10))
	    .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btnContinuarpreguntasCandado']")));
	    element.sendKeys(Keys.ENTER);
	   
	    Thread.sleep(5000);
	    //Seleccion de tarjeta

	    element = (new WebDriverWait(driver,10))
	    .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='oferta1']/div/div/div[1]/div[4]/div/button")));
	    element.click();
	    
	    File scrFile3 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	  	FileUtils.copyFile(scrFile3, new File("c:\\test\\tarjetas\\Paso3.JPG"));
	  	
	    Thread.sleep(5000);
	    
	    //Datos de tarjeta
	    
	    driver.findElement(By.xpath("//*[@id='divdatosSolicitud']/div[5]/div[1]/label/label")).click();
	    
	    driver.findElement(By.id("txtDireccion2")).sendKeys("san");
	    
	    driver.findElement(By.xpath("//*[@id='divdatosSolicitud']/div[7]/div[1]/div[2]/div/div/div[2]/div/div[1]")).click();
	    
	    Thread.sleep(1000);
	    
	    driver.findElement(By.xpath("//*[@id='divdatosSolicitud']/div[7]/div[2]/div[2]/div/div/button")).click();
	    
	    Thread.sleep(1000);
	    
	    driver.findElement(By.xpath("//*[@id='divdatosSolicitud']/div[7]/div[2]/div[2]/div/div/div/ul/li[1]/a")).click();
	    
	    Thread.sleep(1000);
	    
	    driver.findElement(By.xpath("//*[@id='chkDatosSolicitud2']/i")).click();
	    
	    driver.findElement(By.id("btnContinuarDatosSolicitud")).click();
	    
	    Thread.sleep(10000);
	    
	    
	    
	    
	    
	    
//	    String descriptionTextXPath ="";
//	    String descriptionText ="";
//		// Assertt 1
//		String final102_1 = "Estamos enviando tu solicitud para que sea evaluada, de contar con una oferta que se ajuste a ti, un ejecutivo especializado te contactará para guiarte en el proceso.";
//		descriptionTextXPath = "//*[@id='mensajeFinal']";
//		WebElement Element1 = driver.findElement(By.xpath(descriptionTextXPath));
//		descriptionText = Element1.getAttribute("innerText");
//		Assert.assertEquals(final102_1, descriptionText);
//		System.out.println(descriptionText);
//		
//		// Assertt 2
//		String final102_2 = "Para mayor información de los términos y condiciones del producto ingresa a www.bbvacontinental.pe";
//		descriptionTextXPath = "//*[@id='divpasoFin']/div[4]/div[3]/div[5]/div";
//		WebElement Element2 = driver.findElement(By.xpath(descriptionTextXPath));
//		descriptionText = Element2.getAttribute("innerText");
//		Assert.assertEquals(final102_2, descriptionText);
//		System.out.println(descriptionText);
//		
//		// Assertt 3
//		String url102 ="flujo=102";
//		String url=driver.getCurrentUrl();//obtener url actual
//		URL aURL = new URL(url);
//		String ruta102 = aURL.getQuery();
//		ruta102=ruta102.substring(0,9);
//		Assert.assertEquals(url102,ruta102);

		 

	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.close();
	}

}
