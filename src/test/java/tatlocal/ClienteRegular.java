package tatlocal;

import org.testng.Assert;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.io.FileUtils;

public class ClienteRegular {
	WebDriver driver;
	WebElement element;
	String baseUrl;
	
	@BeforeTest
	public void setUp() {
	// init your webdriver
		System.setProperty("webdriver.chrome.driver", "C:\\driver1\\chromedriver.exe");
		
		 driver = new ChromeDriver();
	     DesiredCapabilities capabilities = new DesiredCapabilities();
	     capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	     
	}
	
	@Test
	public void flujoPrincipal() throws Exception {
	    baseUrl = "https://200.107.164.148/tarjetas/";
	    //baseUrl = "https://extranetdev.grupobbva.pe/tarjetas";
		driver.get(baseUrl);
//		driver.manage().window().maximize();
		// Pantalla Inicial

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.id("txtNumeroDocumento")).clear();
		driver.findElement(By.id("txtNumeroDocumento")).sendKeys("15628294");
		driver.findElement(By.id("txtTelefono")).clear();
		driver.findElement(By.id("txtTelefono")).sendKeys("966707745");
		driver.findElement(By.id("txtCorreo")).clear();
		driver.findElement(By.id("txtCorreo")).sendKeys("moraivanm@gmail.com");
		element=driver.findElement(By.id("txtCorreo"));
		element.sendKeys(Keys.TAB);
	    element = (new WebDriverWait(driver,10))
	   .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btnContinuarPaso0']")));
	    element.click();
		File scrFile1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile1, new File("c:\\test\\tarjetas\\Paso1.JPG"));

		Thread.sleep(7000);
		
		//Pantalla Perfilado
		   
	    //Seleccionar característica
	    element=driver.findElement(By.xpath("//*[@id='caracteristica1']"));
	    element.click();
	    
	    driver.findElement(By.xpath("//*[@id='sms-digito-1']")).sendKeys("0");
	    driver.findElement(By.xpath("//*[@id='sms-digito-2']")).sendKeys("0");
	    driver.findElement(By.xpath("//*[@id='sms-digito-3']")).sendKeys("0");
	    driver.findElement(By.xpath("//*[@id='sms-digito-4']")).sendKeys("0");
	    driver.findElement(By.xpath("//*[@id='sms-digito-5']")).sendKeys("0");
	    driver.findElement(By.xpath("//*[@id='sms-digito-6']")).sendKeys("0");
	    
	    element= driver.findElement(By.id("sms-digito-6"));
	    element.sendKeys(Keys.TAB);
	    
	    File scrFile2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	  	FileUtils.copyFile(scrFile2, new File("c:\\test\\tarjetas\\Paso2.JPG"));
	    
	    element = (new WebDriverWait(driver,10))
	    .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='btnContinuarPerfilador']")));
	    element.sendKeys(Keys.ENTER);
	   
	    Thread.sleep(3000);
	  //Llenar datos para Riesgos
       
	    //sexo
	    WebDriverWait wait = new WebDriverWait(driver, 10);
	    JavascriptExecutor js1 = ((JavascriptExecutor) driver);
	    element = driver.findElement(By.id("formSexo"));  
	    js1.executeScript("arguments[0].scrollIntoView(true);", element);
	    Thread.sleep(1000);
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='formSexo']/div/button"))).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id='formSexo']/div/div/ul/li[1]/a/span[1]")).click();
	   //driver.findElement(By.xpath("//*[@id='formSexo']/div/div/ul/li[1]/a/span[1]")).sendKeys(Keys.TAB);
	    //Edad
	    driver.findElement((By.id("txtEdad"))).sendKeys("25");
	    
	    //Estado Civil
//	    element = driver.findElement(By.id("formEstadoCivil"));  
//	    js1.executeScript("arguments[0].scrollIntoView(true);", element);
//	    Thread.sleep(1000);
	    //Estado Civil
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='formEstadoCivil']/div/button"))).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id='formEstadoCivil']/div/div/ul/li[1]/a/span[1]")).click();
	    
	    //Selecccionar Nivel de Instrucción
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='formEstudiosA']/div/button"))).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id='formEstudiosA']/div/div/ul/li[1]/a/span[1]")).click();
	    
	    //Selecccionar Profesión
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='formProfesion']/div/button"))).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id='formProfesion']/div/div/ul/li[1]/a/span[1]")).click();
	    
	    //Ingresar Distrito de Residencia  *modificando el elemento de tipo hidden a texto
	    element=driver.findElement(By.id("txtDireccion"));
//	    String js3 = "arguments[0].setAttribute('type', 'text');return arguments[0]";
//	    element  = (WebElement)((JavascriptExecutor) driver).executeScript(js3, element);driver.findElement(By.id("txtDireccion")).click();
	    element.sendKeys("lince");
	    Thread.sleep(1000);
	    element=driver.findElement(By.className("acom-sugerencia"));
	    element.click();
	    
	    //Miembros de Familia
	    driver.findElement((By.id("txtMiembros"))).sendKeys("5");
	    
	    //Situación de Vivienda
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='formSituacionViviendaA']/div/button"))).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id='formSituacionViviendaA']/div/div/ul/li[1]/a")).click();
	    
	    //Situación Laboral-Independiente
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='formSituacionA']/div/button"))).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id='formSituacionA']/div/div/ul/li[1]/a/span[1]")).click();
	    
	    //Actividad Económica-Minería
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='formActividad']/div/button"))).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//*[@id='formActividad']/div/div/ul/li[4]/a")).click();
	    
	    //Ingresos Fijos
	    driver.findElement((By.id("txtIngFijos"))).sendKeys("8000");
	    
	    //Ingresos Variables
	    driver.findElement((By.id("txtIngVar"))).sendKeys("1000");
	    
	    //Continuar a Pantalla Final
	    driver.findElement(By.id("btnContinuardatosRiesgo1")).sendKeys(Keys.ENTER);
	    
	    Thread.sleep(3000);
	   
	    String descriptionTextXPath ="";
	    String descriptionText ="";
		// Assertt 1
		String final102_1 = "Estamos enviando tu solicitud para que sea evaluada, de contar con una oferta que se ajuste a ti, un ejecutivo especializado te contactará para guiarte en el proceso.";
		descriptionTextXPath = "//*[@id='mensajeFinal']";
		WebElement Element1 = driver.findElement(By.xpath(descriptionTextXPath));
		descriptionText = Element1.getAttribute("innerText");
		Assert.assertEquals(final102_1, descriptionText);
		System.out.println(descriptionText);
		
		// Assertt 2
		String final102_2 = "Para mayor información de los términos y condiciones del producto ingresa a www.bbvacontinental.pe";
		descriptionTextXPath = "//*[@id='divpasoFin']/div[4]/div[3]/div[5]/div";
		WebElement Element2 = driver.findElement(By.xpath(descriptionTextXPath));
		descriptionText = Element2.getAttribute("innerText");
		Assert.assertEquals(final102_2, descriptionText);
		System.out.println(descriptionText);
		
		// Assertt 3
		String url102 ="flujo=102";
		String url=driver.getCurrentUrl();//obtener url actual
		URL aURL = new URL(url);
		String ruta102 = aURL.getQuery();
		ruta102=ruta102.substring(0,9);
		Assert.assertEquals(url102,ruta102);

		 

	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.close();
	}

}
