#Author: ivanbeto@gmail.com
Feature: Eliminar Flujo 112 Riesgos Tarjetas

  @HappyPath
  Scenario Outline: Validar Unificación flujo Regular 
    Given Ingreso al formulario con un cliente Riesgo Alto 
    When Ingreso los datos del paso uno "<Dni>"
    And Ingreso los datos del paso dos
    And Ingreso los datos del paso tres
    Then Se redirige a la pantalla final de un cliente regular
    And se muestra en la url la descripcion del flujo regular
    
    
        Examples: 
      | Dni |
      | 46349175   |
      | 44069357   |
	  