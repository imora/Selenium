package cuentas;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;

public class TestBase {

    //Declare ThreadLocal Driver (ThreadLocalMap) for ThreadSafe Tests
    protected static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();

    @BeforeMethod
    @Parameters(value={"browser","version","plataforma","versionpt","resolution"})
    public void setupTest (String browser, String version, String plataforma, String versionpt, String resolution) throws MalformedURLException {
        //Set DesiredCapabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();

        //Firefox Profile Settings
        /*if (browser=="firefox") {
            FirefoxProfile profile = new FirefoxProfile();
            //Accept Untrusted Certificates
            profile.setAcceptUntrustedCertificates(true);
            profile.setAssumeUntrustedCertificateIssuer(false);
            //Use No Proxy Settings
            profile.setPreference("network.proxy.type", 0);
            //Set Firefox profile to capabilities
            capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        }*/

        //Set BrowserName
        capabilities.setCapability("browser", browser);
        capabilities.setCapability("browser_version", version);
        capabilities.setCapability("os", plataforma);
        capabilities.setCapability("os_version", versionpt);
        capabilities.setCapability("resolution", resolution);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

        //Set Browser to ThreadLocalMap
       // driver.set(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities));
        driver.set(new RemoteWebDriver(new URL("https://ianroma3:J2RWV7hqntJN28KxmGBL@hub-cloud.browserstack.com/wd/hub"), capabilities));
    }

    public WebDriver getDriver() {
        //Get driver from ThreadLocalMap
        return driver.get();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        getDriver().quit();
    }

    @AfterClass void terminate () {
        //Remove the ThreadLocalMap element
        driver.remove();
    }

}
